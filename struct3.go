package main

import "fmt"

type user struct {
	ID, age                 int
	name, surname, username string
	Pay                     *Payment
}
type Payment struct {
	Salary, Bonus float64
}
 // CONSTRUCTOR
func newUser()*user{
	n := new(user)
	return n
}

// METHODLAR

func (u user)GetFullName()string{
	return u.name + " " + u.surname

}

func (p Payment) GetSalaryWithBonus()float64{
	return p.Salary +  p.Bonus
}

func main(){
	u1 :=user{
		ID: 1,
		age: 21,
		name: "Yusufbeak",
		surname: "Husanov",
		username: "YHusanov",
	    Pay: &Payment{Salary: 300,Bonus: 100},
	}
	fmt.Println(u1.age)
	fmt.Println(u1.Pay.Salary + u1.Pay.Bonus)

\\	fmt.Println(u1.GetFullName())

``	fmt.Println(u1.Pay.GetSalaryWithBonus())
}
