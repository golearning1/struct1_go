package main

import "fmt"

type mol  struct{
	eshak string
	kuchuk string
	ot string 
}

func newMol() *mol{
	n := new(mol)
	return n
}

func newMolBilanNarsa(eshak,kuchuk,ot string)*mol{
	n:=new(mol)
	n.eshak=eshak
	n.kuchuk=kuchuk
	n.ot=ot
	return n
}

func (e mol) eshakniUzi()string{
	return(e.eshak + " " + e.kuchuk)
}
func main(){
	h1 :=mol{eshak: "ongsiz hayvon",kuchuk: "vafodor jonzot",ot: "aqilli jonivor"}
	data :=h1.eshak + " " + h1.kuchuk + " " + h1.ot
	fmt.Println(data)

	h2 :=new(mol)
	h2.eshak="ongsiz hayvon"
	h2.kuchuk="vafodor jonzot"
	h2.ot="aqilli jonivor"
	fmt.Println(h2)

	h3 :=new(mol)
	fmt.Println(h3)
	
	h4 :=newMolBilanNarsa("Mehanti xalol uzi xarom","vafodar","aqilli")
	fmt.Println(h4)

	fmt.Println(h1.eshakniUzi())



	

}