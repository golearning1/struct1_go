package main

import (
	"fmt"
	"strconv"
)

type mashina struct{
	yoqilgi string
	mator string
	balon string
	shestirna int
}
  //CONSTRUCTOR
func new_mashina()*mashina{
	n:=new(mashina)
	return n
}
  //PARAMETRLI CONSTRUCTOR
  func new_mashina_qoshildi(yoqilgi,mator,balon string, shestirna int)* mashina{
	  n :=new(mashina)
	  n.yoqilgi=yoqilgi
	  n.mator=mator
	  n.balon=balon
	  n.shestirna=shestirna
	  fmt.Println(n)
	  return n
  }



func main(){
	h1 :=mashina{yoqilgi: "Yiqilgi_Benzin",mator: "1.6 mator",balon: "balon 14",shestirna: 17}
	data1 :=h1.yoqilgi + " " + h1.mator + " " + h1.balon + " " + strconv.Itoa(h1.shestirna)
	fmt.Println(data1)
	h2 :=mashina{yoqilgi: "Yoqilgi gaz",mator: "mator 1.3",balon: "Balon 13",shestirna: 15}
	data2 :=h2.yoqilgi + " " + h2.mator + " " + h2.balon + " " + strconv.Itoa(h2.shestirna)
	fmt.Println(data2)

	h3 :=new_mashina()
	fmt.Println(h3)

	h4 :=new_mashina_qoshildi("Propan","Russia","Altayshina",21)
	fmt.Println(h4)
}