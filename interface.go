package main

import (
	"fmt"
	"strconv"
)

/*type carFase interface {
	run() bool
	stop() bool
	information() string
}

type car struct {
	Brand, Model, Color string
	Speed               int
	Price               float64
}

type SpecialProduction struct {
	Special bool
}

type ferrari struct {
	car
	SpecialProduction
}

func (_ ferrari) Run() bool {
	return true
}

func (_ ferrari) Stop() bool {
	return true
}

func (f ferrari) information() string{
	r := f.Brand + "\n" + f.Model + "\n" + f.Color + " " +strconv.Itoa(f.Speed) + "\n" + strconv.FormatFloat(f.Price, 'f', -1, 64) + "" + "million $"
    a :="Yes"
	if f.Special{
		fmt.Println(a)
	}
	return r
}


type mersedes struct {
	car
	SpecialProduction
}

func (_ mersedes) Run() bool {
	return true
}

func (_ mersedes) Stop() bool {
	return true
}

func (f mersedes) information() string{
	r := f.Brand + "\n" + f.Model + "\n" + f.Color + " " +strconv.Itoa(f.Speed) + "\n" + strconv.FormatFloat(f.Price, 'f', -1, 64) + "" + "million $"
    a :="Yes"
	if f.Special{
		fmt.Println(a)
	}else{

	}
	return r
}

func main() {
	f :=new(ferrari)
	f.Brand="F45"
	f.Model="Ferrari"
	f.Color="Red"
	f.Speed=300
	f.Price=1.2
	f.Special=true
	fmt.Println(f.information())

    fmt.Println("----------------------------------------------------------------")

	m :=new(mersedes)
	m.Brand="X6"
	m.Model="Mersedes"
	m.Color="Red"
	m.Speed=280
	m.Price=2.0
	m.Special=false
	fmt.Println(m.information())

}*/

type carFase interface {
	Run() bool
	Stop() bool
	information() string
}
type car struct {
	Color, Model, Brand string
	Price               float64
	Speed               int
}
type SpecialProduction struct {
	Special bool
}
type ferrari struct {
	car
	SpecialProduction
}

func (_ ferrari)Run()bool{
	return true
}
func (_ ferrari)Stop()bool{
	return true
}
func (f ferrari)information()string{
	r := f.Brand + "\n" + f.Color + "\n" + f.Model + "\n" + strconv.Itoa(f.Speed) + "\n" + strconv.FormatFloat(f.Price, 'f',1,64) + "Million $"
    a :="Yes"
	if f.Special{
		fmt.Println(a)
	}else{

	}
	return r
}


type mersedes struct {
	car
	SpecialProduction
}
func (_ mersedes)Run()bool{
	return true
}
func (_ mersedes)Stop()bool{
	return true
}
func (m mersedes)information()string{
	r := m.Brand + "\n" + m.Color + "\n" + m.Model + "\n" + strconv.Itoa(m.Speed) + "\n" + strconv.FormatFloat(m.Price, 'f',1,64) + "Million $"
	a :="Yes"
	if m.Special{
		fmt.Println(a)
	}else{

	}
	return r
}


func main(){
	f :=new(ferrari)
	f.Brand="F45"
	f.Color="Green"
	f.Model="Ferrari"
	f.Speed=360
	f.Price=2.8
	f.Special=true
	fmt.Println(f.information())
	
	fmt.Println("_____________________________________________________________")
    
	m :=new(mersedes)
	m.Brand="M75"
	m.Color="Yelow"
	m.Model="Mersedes"
	m.Speed=280
	m.Price=2.5
	m.Special=false
	fmt.Println(m.information())

    
}
