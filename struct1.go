package main

import (
	"fmt"
	"strconv"
)

type inson struct{
	name string
	username string
	age int

}
   // CONSTRUCTOR
   func newInson() *inson {
	   n :=new(inson)
	   return n
   }

   //PARAMETRLI CONSTRUCTOR

   func newInsonBilanParametr(name, username string, age int) *inson{
	   n :=new(inson)
	   fmt.Println("CONSTRUCTOR")
	   n.name =name 
	   n.username=username
	   n.age=age
	   return n
   }


func main(){
	h1 := inson {name: "Yusufbek",username: "YHusanov",age: 22 }
	data1 :=h1.name + " " + h1.username + " " + strconv.Itoa(h1.age)
	fmt.Println(data1)
	h2 :=inson {name: "Bektosh", username: "QBektosh",age:22}
	data2 :=h2.name + " " + h2.username + " " + strconv.Itoa(h2.age)
	fmt.Println(data2)

	var h3 = new(inson)
	h3.name="Yusufbek"
	h3.username="YHusanov"
	h3.age=22
	fmt.Println(h3)

	h4 :=new(inson)
	fmt.Println(h4)

	h5 :=newInsonBilanParametr("Laziz","Laziza",15)
	fmt.Println(h5)

}